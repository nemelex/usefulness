defmodule Usefulness.Stream do
  import Stream, only: [drop: 2, take_every: 2]

  @doc """
  Takes only every n-th element from the stream
  after skipping **skip** elements.

  ## Examples
      iex> every_nth_element(0..20, 2) |> Enum.to_list
      [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
      iex> every_nth_element(0..20, 2, 1) |> Enum.to_list
      [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
      iex> every_nth_element(0..20, 2, 3) |> Enum.to_list
      [3, 5, 7, 9, 11, 13, 15, 17, 19]
      iex> every_nth_element(0..20, 5, 2) |> Enum.to_list
      [2, 7, 12, 17]
  """
  def every_nth_element(stream, n, skip \\ 0) when n > 0 and skip >= 0 do
    stream |> drop(skip) |> take_every(n)
  end

  @doc """
  Zips the stream of strings with their lengths.

  ## Examples
      iex> with_length(["1234", "aaa", "bb", "", "55555"]) |> Enum.to_list
      [{"1234", 4}, {"aaa", 3}, {"bb", 2}, {"", 0}, {"55555", 5}]
  """
  def with_length(stream) do
    Stream.zip(stream, Stream.map(stream, &String.length/1))
  end
end
