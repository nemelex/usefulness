defmodule Usefulness.String do
  @doc """
  Checks whether string is a single letter.

  ## Examples

      iex> letter? "a"
      true
      iex> letter? "aa"
      false
      iex> letter? "グ"
      true
      iex> letter? "é"
      true
      iex> letter? "g̈"
      true
      iex> letter? "-"
      false
      iex> letter? "a "
      false
  """
  def letter?(string) do
    Regex.match?(~r/\A\p{L}\p{M}*\z/u, string)
    # match letter codepoint and any number of combining codepoints after it
  end

  @doc ~S"""
  Checks whether string is a single unicode grapheme

  ## Examples

      iex> single_grapheme? "a"
      true
      iex> single_grapheme? "-"
      true
      iex> single_grapheme? " "
      true
      iex> single_grapheme? "é"
      true
      iex> single_grapheme? "グ"
      true
      iex> single_grapheme? "\n"
      true
      iex> single_grapheme? ""
      false
      iex> single_grapheme? "  "
      false
      iex> single_grapheme? "ab"
      false
      iex> single_grapheme? "34"
      false
      iex> single_grapheme? "\na"
      false
      iex> single_grapheme? "a\n"
      false
  """
  def single_grapheme?(string) do
    with graphemes = String.graphemes(string)
    do
      length(graphemes) == 1 and hd(graphemes) == string
    end
  end
end
