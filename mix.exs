defmodule Usefulness.Mixfile do
  use Mix.Project

  def project do
    [
      app: :usefulness,
      version: "0.0.7",
      elixir: "~> 1.2",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      deps: deps,
      package: package,
      source_url: package[:links]["Bitbucket"],
      description: "Useful things"
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:ex_doc, "~> 0.11", only: :dev},
      {:earmark, ">= 0.0.0", only: :dev}
    ]
  end

  defp package do
    [
      files: ["lib", "config", "test", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["Philip B."],
      licenses: ["MIT"],
      links: %{
        "Bitbucket" => "https://bitbucket.org/nemelex/usefulness"
      }
    ]
  end
end
